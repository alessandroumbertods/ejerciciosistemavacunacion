/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import static java.lang.Math.random;
import java.time.LocalDateTime;
import java.util.Random;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madarme
 */
public class SistemaNacionalVacunacion {

    private int etapa;
    private String urlDpto, urlMunicipio, urlPersona, urlContenedor;
    private final ListaCD<Departamento> dptos = new ListaCD();
    private final ListaCD<NotificacionVacunado> registros = new ListaCD();
    private Proveedor[] proveedores;

    public SistemaNacionalVacunacion() {
        this.urlPersona = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/personas.csv";
        this.urlDpto = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv";
        this.urlMunicipio = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv";
        this.urlContenedor = "https://gitlab.com/madarme/archivos-persistencia/-/raw/master/vacuna/vacuna.csv";
        cargarDepartamento();
        cargarMunicipios();
        cargarPersonas();
        cargarContenedores();
        generarNotificaciones();
    }

    private void cargarPersonas() {
        ArchivoLeerURL file = new ArchivoLeerURL(urlPersona);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int cedula = Integer.parseInt(datos2[0]);
            String nombre = datos2[1];
            LocalDateTime time = getFecha(datos2[2], 0, 1, 2, "-");
            int id_municipio = Integer.parseInt(datos2[3]);
            String email = datos2[4];
            Persona p = new Persona(cedula, nombre, time, email);
            Municipio municipio = getMunicipio(id_municipio);
            if (municipio == null) {
                throw new RuntimeException("Municipio no existe");
            }
            municipio.insertarPersonas(p);
        }
    }

    private void cargarMunicipios() {
        ArchivoLeerURL file = new ArchivoLeerURL(urlMunicipio);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_dpto;id_municipio;nombreMunicipio
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_dpto = Integer.parseInt(datos2[0]);
            int id_municipio = Integer.parseInt(datos2[1]);
            String nombreMunicipio = datos2[2];
            Municipio municipio = new Municipio(id_municipio, nombreMunicipio);
            Departamento dpto = getDepartamento(id_dpto);
            if (dpto == null) {
                throw new RuntimeException("Departamento no existe");
            }
            dpto.insertaMunicipio(municipio);
        }
    }

    private void cargarDepartamento() {
        ArchivoLeerURL file = new ArchivoLeerURL(urlDpto);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_dpto;Nombre Departamento
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_dpto = Integer.parseInt(datos2[0]);
            String nombreDepartamento = datos2[1];
            Departamento dpto = new Departamento(id_dpto, nombreDepartamento);
            this.dptos.insertarAlFinal(dpto);
        }
    }

    private void cargarContenedores() {
        ArchivoLeerURL file = new ArchivoLeerURL(urlContenedor);
        Object v[] = file.leerArchivo();
        this.proveedores = new Proveedor[v.length];
        for (int i = 1; i < v.length; i++) {
            //id_proveedor;nombre_proveedor;dosis;fecha_expiracion
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_proveedor = Integer.parseInt(datos2[0]);
            String nombre_proveedor = datos2[1];
            int dosis = Integer.parseInt(datos2[2]);
            LocalDateTime fecha_expiracion = getFecha(datos2[3], 2, 1, 0, "/");
            Proveedor proveedor = new Proveedor(id_proveedor, nombre_proveedor);
            Vacuna vacuna = new Vacuna(i, fecha_expiracion);
            proveedor.insertarVacunas(vacuna, dosis);
            this.proveedores[i - 1] = proveedor;
        }
    }

    private void generarNotificaciones() {
        for (int i = 0; i < this.dptos.getTamanio(); i++) {
            int notificaciones = 0;
            for (int j = 0; j < this.dptos.get(i).getMunicipios().getTamanio(); j++) {
               while(!this.dptos.get(i).getMunicipios().get(j).getPersonas().esVacia()){
                    Persona persona = null;
                    try {
                        persona = this.dptos.get(i).getMunicipios().get(j).getPersonas().deColar();
                    } catch (Exception e) {
                        NotificacionVacunado notificacion = new NotificacionVacunado(LocalDateTime.now(), null);
                    }finally{
                        notificaciones++;
                        this.dptos.get(i).setVacunados(notificaciones);
                    }
                    int random = new Random().nextInt(this.proveedores.length - 1);
                    NotificacionVacunado notificacion = null;
                    try {
                        Vacuna vacuna = this.proveedores[random].getVacunas().desapilar();
                        notificacion = new NotificacionVacunado(LocalDateTime.now(), persona, vacuna);
                        notificaciones++;
                    } catch (Exception e) {
                        notificacion = new NotificacionVacunado(null);
                    } finally {
                        registros.insertarAlFinal(notificacion);
                         this.dptos.get(i).setVacunados(notificaciones);
                    }
                }
            }
        }

    }

    public LocalDateTime getFecha(String datos, int year, int month, int day, String separador) {
        String datos2[] = datos.split(separador);
        return LocalDateTime.of(Integer.parseInt(datos2[year]), Integer.parseInt(datos2[month]), Integer.parseInt(datos2[day]), 0, 0);
    }

    public String toString() {
        String msg = "Bienvenido al Sistema de Vacunación: \nListado de Departamentos: \n";
        for (Departamento d : this.dptos) {
            msg += d.toString() + "\n";
            msg += d.getMunicipios().getTamanio() + "\n";
        }
        return msg;
    }

    private Departamento getDepartamento(int id) {
        for (Departamento dd : this.dptos) {
            if (dd.getId_dpto() == id) {
                return dd;
            }
        }
        return null;
    }

    private Municipio getMunicipio(int id) {
        for (Departamento dd : this.dptos) {
            Municipio m = dd.getMunicipio(id);
            if (m != null) {
                return m;
            }
        }
        return null;
    }

    public String listadoNotificaciones() {
        String msg = "";
        for (NotificacionVacunado r : this.registros) {
            msg += r.toString() + "\n";
        }
        return msg;
    }
    public String dptoMasNotificado(){
        String nombre = this.dptos.get(0).getNombreDpto();
        int notificado = this.dptos.get(0).getVacunados();
        for(Departamento dp: this.dptos){
            if(notificado < dp.getVacunados()){
                nombre = dp.getNombreDpto();
                notificado = dp.getVacunados();
            }
        }
        return nombre + " fue notificado " + notificado + " veces";
    }

}
