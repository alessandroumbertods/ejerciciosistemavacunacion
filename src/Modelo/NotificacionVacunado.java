/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalDateTime;

/**
 *
 * @author madarme
 */
public class NotificacionVacunado {

    private LocalDateTime fechaVacunar;
    private Persona persona_vacunar;
    private Vacuna vacuna_asignada;

    public NotificacionVacunado() {
    }

    public NotificacionVacunado(LocalDateTime fechaVacunar, Persona persona_vacunar, Vacuna vacuna_asignada) {
        this.fechaVacunar = fechaVacunar;
        this.persona_vacunar = persona_vacunar;
        this.vacuna_asignada = vacuna_asignada;
    }

    public NotificacionVacunado(Vacuna vacuna_asignada) {
        this.vacuna_asignada = vacuna_asignada;
    }

    public NotificacionVacunado(LocalDateTime fechaVacunar, Persona persona_vacunar) {
        this.fechaVacunar = fechaVacunar;
        this.persona_vacunar = persona_vacunar;
    }
    

    public LocalDateTime getFechaVacunar() {
        return fechaVacunar;
    }

    public void setFechaVacunar(LocalDateTime fechaVacunar) {
        this.fechaVacunar = fechaVacunar;
    }

    public Persona getPersona_vacunar() {
        return persona_vacunar;
    }

    public void setPersona_vacunar(Persona persona_vacunar) {
        this.persona_vacunar = persona_vacunar;
    }

    public Vacuna getVacuna_asignada() {
        return vacuna_asignada;
    }

    public void setVacuna_asignada(Vacuna vacuna_asignada) {
        this.vacuna_asignada = vacuna_asignada;
    }

    @Override
    public String toString() {
        if(this.vacuna_asignada == null){
            return "No hay más vacunas";
        }
        if(this.persona_vacunar == null){
            return "No hay más personas inscritas para vacunar en el municipio";
        }
        return "NotificacionVacunado{" + "fechaVacunar=" + fechaVacunar + ", persona_vacunar=" + persona_vacunar + ", vacuna_asignada=" + vacuna_asignada + '}';
    }

}
