/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author madar
 */
public class Proveedor {

    private int id_proveedor;
    private String nombre;
    private Pila<Vacuna> vacunas = new Pila();

    public Proveedor() {
    }

    public Proveedor(int id_proveedor, String nombre) {
        this.id_proveedor = id_proveedor;
        this.nombre = nombre;
    }

    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pila<Vacuna> getVacunas() {
        return vacunas;
    }

    public void setVacunas(Pila<Vacuna> vacunas) {
        this.vacunas = vacunas;
    }

    public void insertarVacunas(Vacuna vacuna, int dosis) {
        while (dosis > 0) {
            this.vacunas.apilar(vacuna);
            dosis--;
        }
    }

    @Override
    public String toString() {
        return "Proveedor{" + "id_proveedor=" + id_proveedor + ", nombre=" + nombre + ", vacunas=" + vacunas + '}';
    }

}
