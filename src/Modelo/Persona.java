package Modelo;


import java.time.LocalDateTime;

public class Persona {

    private long cedula;
    private String nombre;
    private LocalDateTime fechaNacimiento;
    private String email;

    public Persona() {
    }

    public Persona(long cedula, String nombre, LocalDateTime fechaNacimiento, String email) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPrioridad(){
         //return LocalDateTime.now().getYear() - this.fechaNacimiento.getYear(); //Prioridad
         return (-1 * (this.getFechaNacimiento().getYear()*10000 +  this.getFechaNacimiento().getMonthValue()*100 + this.getFechaNacimiento().getDayOfMonth()));
    }
    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", fechaNacimiento=" + fechaNacimiento + ", email=" + email + '}';
    }
}
