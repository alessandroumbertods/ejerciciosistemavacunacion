package Modelo;

import ufps.util.colecciones_seed.ListaCD;

public class Departamento {

    private int id_dpto;
    private String nombreDpto;
    private ListaCD<Municipio> municipios=new ListaCD();
    private int vacunados;

    public Departamento() {
    }

    public Departamento(int id_dpto, String nombreDpto) {
        this.id_dpto = id_dpto;
        this.nombreDpto = nombreDpto;
        this.vacunados = 0;
    }

    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public ListaCD<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ListaCD<Municipio> municipios) {
        this.municipios = municipios;
    }

    public int getVacunados() {
        return vacunados;
    }

    public void setVacunados(int vacunados) {
        this.vacunados = vacunados;
    }

    public void insertaMunicipio(Municipio m){
        this.municipios.insertarAlFinal(m);
    }
    
    public Municipio getMunicipio(int id){
        for(Municipio m: this.municipios){
            if(m.getId_municipio() == id){
                return m;
            }
        }
        return null;
    }
    @Override
    public String toString() {
        return "Departamento{" + "id_dpto=" + id_dpto + ", nombreDpto=" + nombreDpto + ", municipios=" + municipios + '}';
    }
    
    
    
}
