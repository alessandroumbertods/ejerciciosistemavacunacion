package Modelo;

import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.ListaCD;

public class Municipio {

    private int id_municipio;
    private String nombre;
    private ColaP<Persona> personas;

    public Municipio() {
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
        this.personas = new ColaP();
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ColaP<Persona> getPersonas() {
        return this.personas;
    }

    public void setPersonas(ColaP<Persona> personas) {
        this.personas = personas;
    }

    public void insertarPersonas(Persona p){
        this.personas.enColar(p, p.getPrioridad());
    }
    @Override
    public String toString() {
        return "Municipio{" + "id_municipio=" + id_municipio + ", nombre=" + nombre + ", personas=" + personas + '}';
    }

    
    
}
